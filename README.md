<!--
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied.
See the License for the specific language governing permissions and
limitations under the License
-->
# SOL003

This repository contains the NB specification of OSM for ETSI NFV SOL003-based API calls.

## Getting Started

Install nodeJS and install the following packages using npm:

```bash
npm install --save @openapi-contrib/json-schema-to-openapi-schema
npm install -g swagger-cli
npm install -g speccy
```

They will be used to test if your changes are conformant to OpenAPI

Run the following to test your changes:

```bash
./devops-stages/stage-test.sh
```

The script will go through all your OpenAPI files and run `swagger-cli validate` and `speccy lint` over them.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://osm.etsi.org/gitweb/?p=osm/TEMPLATE.git;a=tags).

## License

This project is licensed under the Apache2 License - see the [LICENSE.md](LICENSE) file for details

## Acknowledgments

- **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

